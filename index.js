module.exports = (api, options) => {
  api.registerCommand(
    'thanks',
    {
      '--name': 'Who do you want to thank?',
    },
    args => {
      if (args.name) {
        console.log(`Thanks, ${args.name}!`);
      } else {
        console.log('Thanks!');
      }
    },
  );
};
