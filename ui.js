module.exports = api => {
  api.describeTask({
    match: /thanks/,
    description: `Sayin' thanks!`,
    prompts: [
      {
        name: 'name',
        type: 'input',
        message: 'Enter name',
      },
    ],
    onBeforeRun: ({ answers, args }) => {
      if (answers.name) args.push(`--name ${answers.name}`);
    },
  });
};
