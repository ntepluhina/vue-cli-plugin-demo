module.exports = [
  {
    name: 'changeApp',
    type: 'confirm',
    message: 'Modify App.vue?',
    default: false,
  },
  {
    name: 'userName',
    type: 'input',
    message: 'Please enter your name:',
    required: true,
  },
];
