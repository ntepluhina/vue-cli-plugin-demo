module.exports = (api, options) => {
  if (options.changeApp) {
    api.render('./template', {
      ...options,
    });
  }

  api.extendPackage({
    dependencies: {
      '@fortawesome/fontawesome-free': '^5.7.0',
    },
    scripts: {
      thanks: 'vue-cli-service thanks',
    },
  });

  api.injectImports(
    api.entryFile,
    `import '@fortawesome/fontawesome-free/css/all.css'`,
  );
};
